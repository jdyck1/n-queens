def make_board(n):
    row = [0 for i in range(0, n)]
    board = [row.copy() for i in range(0, n)]
    return board

def n_queens(n):
    if n > 3:
        board = make_board(n)
        board[0][2], board[1][0] = 1, 1
        row = 2
        col = n - 1
        c_c = 1
    else:
        return None
    while row < n and col > 0 and col < n:
        board[row][col] = 1
        row += 1
        if col-2 < 1:
            col = n - c_c - 1
            c_c += 1
        elif col == 4 and n % 2 != 0:
            col = 1
        else:
            col -= 2
    return board

def visualize(b, pos, neg):
    # for each row
    # for each item in each row
    # if inner index is at the end
    #   add \n 
    # add item
    s = ""
    for i in range(len(b)):
        for j in range(len(b)):
            t = pos if b[i][j] == 1 else neg
            s += str(t) + " "
            if j == len(b)-1:
                s += "\n"
    return s 

for i in range(4, 15):
    print(visualize(n_queens(i), pos=chr(219), neg='*'))