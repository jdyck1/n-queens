def make_board(n):
    board = []
    for i in range(0, n):
        row = []
        for j in range(0, n):
            row.append(0)
        board.append(row)
    return board


def tile_is_safe(board, x, y):
    w_y = y-1 if y == len(board) else y
    row = [i for i in board[x]]
    col = [r[w_y] for r in board]
    d_down = make_diagonal(board, x, y)['diagonal_down']
    d_up = make_diagonal(board, x, y)['diagonal_up']
    collection = row + col + d_down + d_up 

    for i in collection:
        if i != 0:
            return False 
    return True
    

def back_up(x, y, board):
    c = {'t_x': x, 't_y': y, 'b_x': x, 'b_y': y}

    while c['t_x'] > 0 and c['t_y'] > 0:
        c['t_x'] -= 1
        c['t_y'] -= 1
    while c['b_x'] > 0 and c['b_y'] < len(board)-1:
        c['b_x'] -= 1
        c['b_y'] += 1
    
    return c


def make_diagonal(board, x, y): 
    b = back_up(x, y, board)
    t_x, t_y = b['t_x'], b['t_y']
    b_x, b_y = b['b_x'], b['b_y']
    d_down_t = []
    d_up_b = []

    while t_x < len(board) and t_y < len(board):
        d_down_t.append(board[t_y][t_x])
        t_x += 1
        t_y += 1 
    while b_x < len(board) and b_y > 0:
        d_up_b.append(board[b_y][b_x])
        b_x += 1
        b_y -= 1
    
    return {'diagonal_down': d_down_t, 'diagonal_up': d_up_b}


def place_piece(board, x, y):
    board[x][y] = 1
    return True


def n_queens(n):
    board = make_board(n)
    board[0][2] = 1
    for x in range(0, n):
        for y in range(0, n):
            if tile_is_safe(board, x, y):
                print(board)
                place_piece(board, x, y)

    return board

b = [
    [11,12,13,14],
    [21,22,23,24],
    [31,32,33,34],
    [41,42,43,44]
]

b_2 = [
    [1,0,0,0],
    [0,0,0,0],
    [0,0,0,0],
    [0,0,0,0]
]

# b_3 = make_board(4)

# place_piece(b_3, 1, 1)
# print(b_3)

print('func call', n_queens(8))
# place_piece(b_2, 1, 1)
# print(b_2)
# print(b[1][3])
# print('make_diagonal', make_diagonal(b, 3, 1))
print(tile_is_safe(b_2, 2, 1))